import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyAzuz8dEI5hGJccqRdN96SKqP7Q-r3aluk",
    authDomain: "chat-f5a5b.firebaseapp.com",
    databaseURL: "https://chat-f5a5b.firebaseio.com",
    projectId: "chat-f5a5b",
    storageBucket: "chat-f5a5b.appspot.com",
    messagingSenderId: "894875613243",
    appId: "1:894875613243:web:b82e92fae361032b"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);

  export default firebaseApp.firestore()